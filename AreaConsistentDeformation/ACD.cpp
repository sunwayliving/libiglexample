#define _CRT_SECURE_NO_WARNINGS
#include <igl/colon.h>
#include <igl/directed_edge_orientations.h>
#include <igl/directed_edge_parents.h>
#include <igl/forward_kinematics.h>
#include <igl/PI.h>
#include <igl/lbs_matrix.h>
#include <igl/deform_skeleton.h>
#include <igl/dqs.h>
#include <igl/readDMAT.h>
#include <igl/readOFF.h>
#include <igl/svd3x3/arap.h>
#include <igl/viewer/Viewer.h>
//for pick points
#include <igl/embree/EmbreeIntersector.h>
#include <igl/embree/unproject_onto_mesh.h>

#include <Eigen/Geometry>
#include <Eigen/StdVector>
#include <vector>
#include <algorithm>
#include <iostream>

/*How to use it
1. load a mesh, press "L"
2. use mouse left button to select some points, press "P" to run arap precomputation
3. deselect some of the selected points to act as mathematical boundary, then press "A" to run ARAP
4. You will see the area after each step
NOTES:
1. press "X/Y/Z", then use mouse to move points along axis as you like. press "U" to stop move pick points along all axises
2. press "C" to deselect all the points you just selected
*/

using namespace std;
using namespace Eigen;

typedef
std::vector < Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond> >
RotationList;

const Eigen::RowVector3d sea_green(70. / 255., 252. / 255., 167. / 255.);
Eigen::MatrixXd V, U, bc;
Eigen::MatrixXi F, bcf;
Eigen::VectorXi S, b; //b: 0 ~ v.rows()
Eigen::RowVector3d mid;
double anim_t = 0.0;
double anim_t_dir = 0.03;
double change = 3.f;
igl::ARAPData arap_data;

//for pick points
igl::EmbreeIntersector* ei;
vector<int> picked_vertices;
bool is_move_x = false;
bool is_move_y = false;
bool is_move_z = false;
double old_x = 0., old_y = 0;
double new_x = 0, new_y = 0;

// Per-vertex color
MatrixXd C_Vert;
RowVector3d purple(80.0 / 255.0, 64.0 / 255.0, 255.0 / 255.0);
RowVector3d red(255. / 255., 0., 0.);
RowVector3d gold(255.0 / 255.0, 228.0 / 255.0, 58.0 / 255.0);

//*****Forward Declaration**************
double computeArea(Eigen::MatrixXd &V, Eigen::MatrixXi &F);

void rotatePickedPoints(vector<int> &p_v)
{

}

void translatePickedPoints(igl::Viewer &viewer, vector<int> &p_v){
	if (!is_move_x && !is_move_y && !is_move_z)
		return;
	if (p_v.empty())
		return;

	new_x = viewer.current_mouse_x;
	new_y = viewer.current_mouse_y;

	float move_step = .15;
	Vector2d move_dir;
	move_dir<< new_x - old_x, new_y - old_y;

	if (move_dir.norm() != 0) //non-zero vector can be normalized, zero vector will cause #1.INF
	{
		move_dir.normalize();
	}


	for (int i = 0; i < p_v.size(); ++i)
	{
		if (is_move_x) U(p_v[i], 0) = U(p_v[i], 0) + move_dir.x() * move_step;
		if (is_move_y) U(p_v[i], 1) = U(p_v[i], 1) - 1 * move_dir.y() * move_step;
		if (is_move_z) U(p_v[i], 2) = U(p_v[i], 2) + move_dir.x() * move_step;
	}

	old_x = new_x;
	old_y = new_y;

	viewer.data.set_mesh(U, F);
}

void clearPickedPoints(vector<int> &p_v){
	//清除红色
	for (int i = 0; i < p_v.size(); ++i){
		C_Vert.row(p_v[i]) << 1, 1, 1;
	}
	//清除索引
	p_v.clear();
	return;
}

void applyArap(igl::Viewer & viewer)
{
	//bc的大小要和前面conservativeResize后b的行数一致，就是保证bc是手动变换位置后的固定点数量一致
	MatrixXd bc(b.size(), U.cols());
	//变换bc的位置，然后用arap去计算mesh中其它点的位置
	for (int i = 0; i < b.size(); i++)
	{
		bc.row(i) = U.row(b(i));
	}

	igl::arap_solve(bc, arap_data, U);
	cout << "area after deform(YES ARAP) : " << computeArea(U, F) << endl;
	viewer.data.set_mesh(U, F);
	
	MatrixXd C(F.rows(), 3);
	for (int f = 0; f < F.rows(); f++)
	{
		C.row(f) = gold;
	}
	viewer.data.set_colors(C);
}

void preARAP()
{
	if (picked_vertices.empty())
	{
		cout << "ARAP boundary points empty!\nYou should pick some points as mathematical boundary!" << endl;
		return;
	}

	b.resize(picked_vertices.size());
	for (int i = 0; i < picked_vertices.size(); ++i)
	{
		b(i) = picked_vertices[i];
	}

	arap_data.max_iter = 100;
	igl::arap_precomputation(V, F, V.cols(), b, arap_data);
}

bool key_down(igl::Viewer &viewer, unsigned char key, int mods)
{
	switch (key){
	case 'A':
		cout << "*******run ARAP*******" << endl;
		applyArap(viewer);
		cout << "*******End*******" << endl;
		return true;
	case 'C':
		cout << "clear pick points" << endl;
		clearPickedPoints(picked_vertices);
		viewer.data.set_colors(C_Vert);
		return true;
	case 'L':
		cout << "load mesh from file" << endl;
		viewer.open_dialog_mesh(&viewer);
		//这里要将viewer里面的数据放到 V 和 U里面去, 后面ARAP是基于V和U来做的，结果会放到U中
		//cout << viewer.data.V.rows() << endl;
		V = viewer.data.V;
		U = V;
		return true;
	case 'P':
		cout << "*******Begin to run ARAP precomputation*******" << endl;
		preARAP();//
		cout << "*******End*******" << endl <<endl;
		return true;
	case 'R': //换成r就不行
		cout << "R is pressed " << endl;
		return true;
	case 'S':
		viewer.save_mesh_to_file("viewer_data.off");
		cout << "save current mesh in viewer" << endl;
		return true;
	case 'U':
		cout << "set all move_x/y/z to false" << endl;
		is_move_x = is_move_y = is_move_z = false;
		cout << "area after deform(NO ARAP) : " << computeArea(U, F) << endl <<endl;
		return true;
	case 'X':
		old_x = viewer.current_mouse_x;
		old_y = viewer.current_mouse_y;
		is_move_x = !is_move_x;
		is_move_x ? cout << "Begin to move along X Axis" << endl : cout << "Stop to move along X Axis" << endl <<endl;
		return true;
	case 'Y':
		old_x = viewer.current_mouse_x;
		old_y = viewer.current_mouse_y;
		is_move_y = !is_move_y;
		is_move_y ? cout << "Begin to move along X Axis" << endl : cout << "Stop to move along Y Axis" << endl <<endl;
		return true;
	case 'Z':
		old_x = viewer.current_mouse_x;
		old_y = viewer.current_mouse_y;
		is_move_z = !is_move_z;
		is_move_z ? cout << "Begin to move along X Axis" << endl : cout << "Stop to move along Z Axis" << endl <<endl;
		return true;
	default:
		break;
	}
	return false;
}

bool mouse_down(igl::Viewer& viewer, int button, int modifier)
{
	new_x = viewer.current_mouse_x;
	new_y = viewer.current_mouse_y;

	int vid, fid;

	// Cast a ray in the view direction starting from the mouse position
	double x = viewer.current_mouse_x;
	double y = viewer.core.viewport(3) - viewer.current_mouse_y;
	bool hit = igl::unproject_onto_mesh(Vector2f(x, y),
		F,
		viewer.core.view * viewer.core.model,
		viewer.core.proj,
		viewer.core.viewport,
		*ei,
		fid,
		vid);

	// If the ray hits a face, assign a red color to the closest vertex
	if (hit)
	{
		cerr << "Picked face(vertex): " << fid << " (" << vid << ")" << endl;
		vector<int>::iterator it = std::find(picked_vertices.begin(), picked_vertices.end(), vid);
		if ( it!= picked_vertices.end())//found, then delete
		{
			picked_vertices.erase(it);
			C_Vert.row(vid) << 1, 1, 1;
		}else{
			picked_vertices.push_back(vid);
			C_Vert.row(vid) << 1, 0, 0;
		}

		viewer.data.set_colors(C_Vert);
		return true;
	}

	return false;
}

bool mouse_move(igl::Viewer& viewer, int button, int modifier)
{
	translatePickedPoints(viewer, picked_vertices);
	return false;//返回true的话，在viewer的源代码中就不能执行其他plugin的鼠标处理函数了
}

//***************************************************************
double computeArea(Eigen::MatrixXd &V, Eigen::MatrixXi &F){
	//API for Compute Area
	VectorXd area;
	igl::doublearea(V, F, area);
	double area_sum = area.sum();
	return area_sum;

	/*double area = 0.0f;
	for (int f = 0; f < F.rows(); f++)
	{
	Eigen::Vector3d v0 = V.row(F(f, 0));
	Eigen::Vector3d v1 = V.row(F(f, 1));
	Eigen::Vector3d v2 = V.row(F(f, 2));

	Eigen::Vector3d e1 = v1 - v0;
	Eigen::Vector3d e2 = v2 - v0;
	area = area + (e1.cross(e2)).norm();
	}
	return area;*/
}

int main(int argc, char *argv[])
{
	using namespace Eigen;
	using namespace std;

	igl::readOFF("../data/bottle.off", V, F);
	U = V;
	cout << "Area before deform: " << computeArea(V, F) << endl;
	//b = boundary points

	//for pick points, 如果换mesh的话，可能要重新 new一个并init
	ei = new igl::EmbreeIntersector();
	ei->init(V.cast<float>(), F);
	//color
	C_Vert = MatrixXd::Constant(V.rows(), 3, 1);


	MatrixXd C(F.rows(), 3);
	for (int f = 0; f < F.rows(); f++)
	{
		C.row(f) = gold;
	}

	igl::Viewer viewer;
	viewer.data.set_mesh(U, F);
	viewer.data.set_colors(C);

	viewer.callback_key_down = &key_down;
	//viewer.callback_key_up = &key_up; //当鼠标连按的时候，会一直有key_up事件发送，不明缘故
	viewer.callback_mouse_down = &mouse_down;
	viewer.callback_mouse_move = &mouse_move;
	viewer.core.animation_max_fps = 30.;
	cout << "Press [space] to run ARAP" << endl;
	viewer.launch();
	return 0;
}