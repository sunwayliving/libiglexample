#define _CRT_SECURE_NO_WARNINGS
#include <igl/colon.h>
#include <igl/directed_edge_orientations.h>
#include <igl/directed_edge_parents.h>
#include <igl/forward_kinematics.h>
#include <igl/PI.h>
#include <igl/lbs_matrix.h>
#include <igl/deform_skeleton.h>
#include <igl/dqs.h>
#include <igl/readDMAT.h>
#include <igl/readOFF.h>
#include <igl/svd3x3/arap.h>
#include <igl/viewer/Viewer.h>

#include <Eigen/Geometry>
#include <Eigen/StdVector>
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;
using namespace Eigen;

typedef
std::vector < Eigen::Quaterniond, Eigen::aligned_allocator<Eigen::Quaterniond> >
RotationList;

const Eigen::RowVector3d sea_green(70. / 255., 252. / 255., 167. / 255.);
Eigen::MatrixXd V, U;
Eigen::MatrixXi F;
Eigen::VectorXi S, b; //b: 0 ~ v.rows()
Eigen::RowVector3d mid;
double anim_t = 0.0;
double anim_t_dir = 0.03;
igl::ARAPData arap_data;

bool pre_draw(igl::Viewer & viewer)
{
	using namespace Eigen;
	using namespace std;
	MatrixXd bc(b.size(), V.cols()); //开辟跟手动选定点的size一样大的空间

	for (int i = 0; i < b.size(); i++)
	{
		bc.row(i) = V.row(b(i)); //b(i)是点的索引，拿到mesh的点的位置
		//根据S中的记录，判断这个点的值，给出相应的pos变化
		//pos变化中的，参数anim_t就是控制mesh随时间变化的
		switch (S(b(i)))
		{
		case 0:
		{
			const double r = mid(0)*0.25;
			bc(i, 0) += r * sin(0.5 * anim_t * 2. * igl::PI);
			bc(i, 1) -= r + r*cos(igl::PI + 0.5*anim_t*2.*igl::PI);
			break;
		}
		case 1:
		{
			const double r = mid(1)*0.15;
			bc(i, 1) += r + r*cos(igl::PI + 0.15*anim_t*2.*igl::PI);
			bc(i, 2) -= r*sin(0.15*anim_t*2.*igl::PI);
			break;
		}
		case 2:
		{
			const double r = mid(1)*0.15;
			bc(i, 2) += r + r*cos(igl::PI + 0.35*anim_t*2.*igl::PI);
			bc(i, 0) += r*sin(0.35*anim_t*2.*igl::PI);
			break;
		}
		default:
			break;
		}
	}
	//当bc变了之后，solve出了一组所有mesh的新的点位置U
	igl::arap_solve(bc, arap_data, U);

	//绘制arap计算出的mesh上所有点的新位置
	viewer.data.set_vertices(U);
	viewer.data.compute_normals();
	//这个不停的在改变constrain point的位置
	if (viewer.core.is_animating)
	{
		anim_t += anim_t_dir;
	}
	return false; 
}

bool key_down(igl::Viewer &viewer, unsigned char key, int mods)
{
	switch (key)
	{
	case ' ':
		viewer.core.is_animating = !viewer.core.is_animating;
		return true;
	}
	return false;
}

//***************************************************************
double computeArea(Eigen::MatrixXd &V, Eigen::MatrixXi &F){
	double area = 0.0f;
	for (int f = 0; f < F.rows(); f++)
	{
		Eigen::Vector3d v0 = V.row(F(f, 0));
		Eigen::Vector3d v1 = V.row(F(f, 1));
		Eigen::Vector3d v2 = V.row(F(f, 2));

		Eigen::Vector3d e1 = v1 - v0;
		Eigen::Vector3d e2 = v2 - v0;
		area = area + (e1.cross(e2)).norm();
	}
	return area;
}

void testEigen(Eigen::MatrixXd &m){
	cout << m.rows() << endl; //矩阵的行数

}

//int main(){
//	igl::readOFF("../shared/decimated-knight.off", V, F);
//	testEigen(V);
//	system("PAUSE");
//	return 0;
//}

int main(int argc, char *argv[])
{
	if (argc < 1){
		cout << argv[0] << endl;
		std::cout << "required 1 args" << endl;
		system("pause");
		return 1;
	}

	using namespace Eigen;
	using namespace std;
	igl::readOFF("../shared/decimated-knight.off", V, F);
	//igl::readOFF("../data/Bottle.obj", V, F);
	//cout << computeArea(V, F) << endl;
	//system("PAUSE");
	//return 0; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	U = V;
	igl::readDMAT("../shared/decimated-knight-selection.dmat", S);

	// vertices in selection
	cout << "v.rows() : " << V.rows() - 1 << endl;
	cout << b << endl;

	igl::colon<int>(0, V.rows() - 1, b); //low = 0, high = V.rows(), 值被放入b中返回

	// b中是点所在的行数，可以看成点的索引。partition的条件就是根据b中下标，找到S中对应行数的值 来判断的。
	// b.data()是b的开始位置的迭代器，stable_partition后返回false组的首个元素的迭代器，相减之后就是元素个数
	cout << "b.size() 1: " << b.size() << endl;
	b.conservativeResize(
		stable_partition(b.data(), b.data() + b.size(), [](int i)->bool{return S(i) >= 0;}) //stable_partition
		- b.data());
	//这一步之后，b中就剩下那些手动选出来的点的索引值
	cout << "************************************************" << endl;
	cout << "b.size() 2: " << b.size() << endl;
	cout << b << endl;

	// Centroid
	mid = 0.5*(V.colwise().maxCoeff() + V.colwise().minCoeff());

	// Precomputation
	arap_data.max_iter = 100;
	igl::arap_precomputation(V, F, V.cols(), b, arap_data);

	// Set color based on selection
	MatrixXd C(F.rows(), 3);
	RowVector3d purple(80.0 / 255.0, 64.0 / 255.0, 255.0 / 255.0);
	RowVector3d red(255. / 255., 0., 0.);
	RowVector3d gold(255.0 / 255.0, 228.0 / 255.0, 58.0 / 255.0);

	for (int f = 0; f < F.rows(); f++)
	{
		if (S(F(f, 0)) >= 0 && S(F(f, 1)) >= 0 && S(F(f, 2)) >= 0)
		{
			C.row(f) = red;
		}
		else
		{
			C.row(f) = gold;
		}
	}

	// Plot the mesh with pseudocolors
	igl::Viewer viewer;
	viewer.data.set_mesh(U, F);
	viewer.data.set_colors(C);
	viewer.callback_pre_draw = &pre_draw;
	viewer.callback_key_down = &key_down;
	viewer.core.is_animating = false;
	viewer.core.animation_max_fps = 30.;
	cout <<
		"Press [space] to toggle animation" << endl;
	viewer.launch();
}